class SaneBackends < Formula
  desc "Backends for scanner access - with auto-configuration of unconfigured devices"
  homepage "http://www.sane-project.org/"
  url "https://gitlab.com/nikkoura.public/backends/-/archive/1.0.27_5_1/backends-1.0.27_5_1.tar.gz"
  sha256 "106c322688e064d5edbaa101879c9a38b358b38c648550d85c7b100ffd0a1b85"
  version "1.0.27_5_1"
  revision 1
  head "https://gitlab.com/nikkoura.public/backends.git"

  depends_on "pkg-config" => :build
  depends_on "jpeg"
  depends_on "libpng"
  depends_on "libtiff"
  depends_on "libusb"
  depends_on "net-snmp"
  depends_on "openssl"

  def install
    ENV["CPPFLAGS"] = "-I/usr/include/malloc"
    inreplace "backend/artec_eplus48u.conf.in", "@DATADIR@/sane", "#{HOMEBREW_PREFIX}share/sane/firmware/"
    inreplace "backend/epjitsu.conf.in", "@DATADIR@/sane", "#{HOMEBREW_PREFIX}share/sane/firmware/"
    inreplace "backend/gt68xx.conf.in", "@DATADIR@/sane", "#{HOMEBREW_PREFIX}share/sane/firmware/"
    inreplace "backend/snapscan.conf.in", "@DATADIR@/sane", "#{HOMEBREW_PREFIX}share/sane/firmware/"

    system "./configure", "--disable-dependency-tracking",
                          "--prefix=#{prefix}",
                          "--localstatedir=#{var}",
                          "--without-gphoto2",
                          "--enable-local-backends",
                          "--with-usb=yes"

    # Remove for > 1.0.27
    # Workaround for bug in Makefile.am described here:
    # https://lists.alioth.debian.org/pipermail/sane-devel/2017-August/035576.html
    # It's already fixed in commit 519ff57.
    system "make"
    system "make", "install"
  end

  def post_install
    # Some drivers require a lockfile
    (var/"lock/sane").mkpath

    # Some drivers require a firmware, the user will have to provide it in these
    # directories
    (HOMEBREW_PREFIX/"share/sane/firmware/").mkpath
    (HOMEBREW_PREFIX/"share/sane/firmware/artec_eplus48u").mkpath
    (HOMEBREW_PREFIX/"share/sane/firmware/epjitsu").mkpath
    (HOMEBREW_PREFIX/"share/sane/firmware/gt68xx").mkpath
    (HOMEBREW_PREFIX/"share/sane/firmware/snapscan").mkpath
  end

  test do
    assert_match prefix.to_s, shell_output("#{bin}/sane-config --prefix")
  end
end
