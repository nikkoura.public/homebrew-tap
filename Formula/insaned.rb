class Insaned < Formula
  desc "Simple daemon for polling button presses on SANE scanners"
  homepage "https://github.com/abusenius/insaned"
  url "https://github.com/nikkoura/insaned/archive/v0.0.3_1.tar.gz"
  sha256 "8e450f2bc73c02f1a84f69140f5b567f1dd33c1e674480fc5cbb4b9a1942104c"
  head "https://github.com/nikkoura/insaned.git"
  version "v0.0.3_1"

  depends_on "cmake" => :build
  depends_on "sane-backends" => :recommended

  def install
    system "cmake", ".", *std_cmake_args
    system "make", "install"
  end
end
